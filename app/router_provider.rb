require 'faraday'
require_relative './invalid_coordenates_error'

class RouterProvider
  def initialize(api_key, api_url)
    @api_key = api_key
    @api_url = api_url || 'https://router.hereapi.com'
  end

  def duration(trip, return_type = 'summary')
    http_response = Faraday.get("#{@api_url}/v8/routes?apiKey=#{@api_key}&return=#{return_type}&transportMode=#{trip.transport_mode}&origin=#{trip.origin}&destination=#{trip.destination}")
    return hash_duration(http_response) if http_response.success?

    raise StandardError, "Error en la solicitud HTTP: #{response.status}"
  end

  private

  def hash_duration(response_json)
    hash_response = JSON.parse(response_json.body)
    # siempre devuelvo cantidad entera de minutos redondeando para arriba
    if hash_response['routes'][0] && hash_response['routes'][0]['sections']
      (hash_response['routes'][0]['sections'][0]['summary']['duration'] / 60).ceil
    else
      raise InvalidCoordinateError
    end
  end
end
