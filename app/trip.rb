class Trip
  TRANSPORT_MODE = 'car'.freeze
  attr_reader :latitude1, :longitude1, :latitude2, :longitude2

  def initialize(latitude1, longitude1, latitude2, longitude2)
    validate_coordinates(latitude1, longitude1, latitude2, longitude2)

    # pasarlo a entidad Coordenate
    @latitude1 = latitude1
    @longitude1 = longitude1
    @latitude2 = latitude2
    @longitude2 = longitude2
  end

  def valid?
    valid_coordinate?(@latitude1) && valid_coordinate?(@longitude1) && valid_coordinate?(@latitude2) && valid_coordinate?(@longitude2)
  end

  def validate_coordinates(latitude1, longitude1, latitude2, longitude2)
    raise InvalidCoordinateError unless valid_coordinate?(latitude1) && valid_coordinate?(longitude1) && valid_coordinate?(latitude2) && valid_coordinate?(longitude2)
  end

  def valid_coordinate?(coordinate)
    coordinate.is_a?(Numeric)
  end

  def origin
    "#{@latitude1},#{@longitude1}"
  end

  def destination
    "#{@latitude2},#{@longitude2}"
  end

  def transport_mode
    TRANSPORT_MODE
  end
end
