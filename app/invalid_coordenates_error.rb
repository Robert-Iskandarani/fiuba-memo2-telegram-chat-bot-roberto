class InvalidCoordinateError < StandardError
  def initialize(message = 'Invalid latitude or longitude')
    super(message)
  end
end
