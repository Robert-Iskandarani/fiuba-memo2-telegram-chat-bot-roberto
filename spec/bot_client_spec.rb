require 'spec_helper'
require 'web_mock'
# Uncomment to use VCR
# require 'vcr_helper'

require "#{File.dirname(__FILE__)}/../app/bot_client"

def when_i_send_text(token, message_text)
  body = { "ok": true, "result": [{ "update_id": 693_981_718,
                                    "message": { "message_id": 11,
                                                 "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                                                 "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                                                 "date": 1_557_782_998, "text": message_text,
                                                 "entities": [{ "offset": 0, "length": 6, "type": 'bot_command' }] } }] }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def when_i_send_keyboard_updates(token, message_text, inline_selection)
  body = {
    "ok": true, "result": [{
      "update_id": 866_033_907,
      "callback_query": { "id": '608740940475689651', "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                          "message": {
                            "message_id": 626,
                            "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                            "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                            "date": 1_595_282_006,
                            "text": message_text,
                            "reply_markup": {
                              "inline_keyboard": [
                                [{ "text": 'Jon Snow', "callback_data": '1' }],
                                [{ "text": 'Daenerys Targaryen', "callback_data": '2' }],
                                [{ "text": 'Ned Stark', "callback_data": '3' }]
                              ]
                            }
                          },
                          "chat_instance": '2671782303129352872',
                          "data": inline_selection }
    }]
  }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def then_i_get_text(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544', 'text' => message_text }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def then_i_get_keyboard_message(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544',
              'reply_markup' => '{"inline_keyboard":[[{"text":"Jon Snow","callback_data":"1"},{"text":"Daenerys Targaryen","callback_data":"2"},{"text":"Ned Stark","callback_data":"3"}]]}',
              'text' => 'Quien se queda con el trono?' }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def when_here_answers(api_key, duration)
  body = { "routes": [{ "id": '1793a897-0843-4957-ab63-c61e0f13aff2',
                        "sections": [{ "id": 'c3e300ab-a880-4fab-9bb2-702355b2bf6b', "type": 'vehicle',
                                       "actions": [{ "action": 'depart', "duration": 126,
                                                     "instruction": 'Head toward Chausseestraße on Invalidenstraße. Go for 1.2 km.',
                                                     "offset": 0 },
                                                   { "action": 'arrive', "duration": 0,
                                                     "instruction": 'Arrive at Invalidenstraße.',
                                                     "offset": 78 }],
                                       "departure": { "time": '2019-12-05T15:15:56+01:00',
                                                      "place": { "type": 'place', "location": { "lat": 52.53100287169218,
                                                                                                "lng": 13.38464098982513 } } },
                                       "arrival": { "time": '2019-12-05T15:18:02+01:00',
                                                    "place": { "type": 'place', "location": { "lat": 52.52639072947204,
                                                                                              "lng": 13.368653766810894 } } },
                                       "summary": { "duration": duration, "length": 1200 },
                                       "polyline": 'BG2znmkDi89wZ9ChKAA1IvfAArH5cAArHvbAA1CrJAArF5SAAtP9yBAAT1E3E3QAA_BrH3M9sBAA_F5SAA3KlkBAA1EtNAApB_DAAhC1EAApB1I_D5OAA3ErPAApFtTAAtN_'\
                                         'wBAA1GtVAA5U3lCAA_DhOAA3KliBAAtXjvCAArDtLAA1EhQAA1CrJAA_BrFAAvbl9CAAhIvZ_FtTrDtLAAV1I1CtNAA1E3QAArLnoB1G5YAAhGhSpBrFAAhC1GAA1FxT',
                                       "spans": [{ "offset": 0, "names": [{ "value": 'Invalidenstraße', "language": 'de' }], "length": 787 },
                                                 { "offset": 49, "names": [{ "value": 'Invalidenstraße', "language": 'de' },
                                                                           { "value": 'Sandkrugbrücke', "language": 'de' }], "length": 51 },
                                                 { "offset": 57, "names": [{ "value": 'Invalidenstraße', "language": 'de' }], "length": 362 }], "transport": { "mode": 'car' } }] }] }

  stub_request(:get, "https://router.hereapi.com/v8/routes?apiKey=#{api_key}&destination=52.52639072947204,13.368653766810894&origin=52.53100287169218,13.38464098982513&return=summary&transportMode=car")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

describe 'BotClient' do
  it 'should get a /version message and respond with current version' do
    token = 'fake_token'

    when_i_send_text(token, '/version')
    then_i_get_text(token, Version.current)

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /say_hi message and respond with Hola Emilio' do
    token = 'fake_token'

    when_i_send_text(token, '/say_hi Emilio')
    then_i_get_text(token, 'Hola, Emilio')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /start message and respond with Hola' do
    token = 'fake_token'

    when_i_send_text(token, '/start')
    then_i_get_text(token, 'Hola, Emilio')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /stop message and respond with Chau' do
    token = 'fake_token'

    when_i_send_text(token, '/stop')
    then_i_get_text(token, 'Chau, egutter')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /tv message and respond with an inline keyboard' do
    token = 'fake_token'

    when_i_send_text(token, '/tv')
    then_i_get_keyboard_message(token, 'Quien se queda con el trono?')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a "Quien se queda con el trono?" message and respond with' do
    token = 'fake_token'

    when_i_send_keyboard_updates(token, 'Quien se queda con el trono?', '2')
    then_i_get_text(token, 'A mi también me encantan los dragones!')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get an unknown message message and respond with Do not understand' do
    token = 'fake_token'

    when_i_send_text(token, '/unknown')
    then_i_get_text(token, 'Uh? No te entiendo! Me repetis la pregunta?')

    app = BotClient.new(token)

    app.run_once
  end

  # rubocop:disable RSpec/ExampleLength
  it 'should get a /viaje message and respond with travel time' do
    token = 'fake_token'
    here_api_key = ENV['HERE_API_KEY']

    when_i_send_text(token, '/viaje 52.53100287169218,13.38464098982513,52.52639072947204,13.368653766810894')
    when_here_answers(here_api_key, 188)
    then_i_get_text(token, 'Vas a tardar 3 minutos en hacer tu viaje en auto')

    app = BotClient.new(token)

    app.run_once
  end
  # rubocop:enable RSpec/ExampleLength
end
