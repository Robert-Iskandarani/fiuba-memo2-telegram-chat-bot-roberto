require_relative '../app/trip'
require_relative '../app/invalid_coordenates_error'

describe 'Trip' do
  it 'should initialize with correct coordinates' do
    trip = Trip.new(52.53100287169218, 13.38464098982513, 52.52639072947204, 13.368653766810894)

    expect(trip).to be_valid
  end

  it 'should raise InvalidCoordinateError for invalid coordinates' do
    expect { Trip.new('invalid', 13.38464098982513, 52.52639072947204, 13.368653766810894) }.to raise_error(InvalidCoordinateError)
  end

  describe 'Coordinates' do
    let(:trip) { Trip.new(52.53100287169218, 13.38464098982513, 52.52639072947204, 13.368653766810894) }

    it 'should return origin coordinates correctly' do
      expect(trip.origin).to eq '52.53100287169218,13.38464098982513'
    end

    it 'should return destination coordinates correctly' do
      expect(trip.destination).to eq '52.52639072947204,13.368653766810894'
    end
  end
end
