require_relative '../app/router_provider'
require 'dotenv'
Dotenv.load('.env')

describe 'RouterProvider' do
  xit 'should invoke here api' do
    WebMock.disable!
    router_provider = RouterProvider.new(ENV['HERE_API_KEY'], nil)
    trip = Trip.new(52.53100287169218, 13.38464098982513, 52.52639072947204, 13.368653766810894)
    expect(router_provider.duration(trip)).to be >= 0
    WebMock.enable!
  end
end
